# This plain-text version includes comments and
# separate NPUSH instructions to improve legibility.

# Push the VALUE that should be displayed.
# It must be positive and the only value on the stack

	MPPEM

# Calculate left digit
# (floor(VALUE/10) * 1/64)

	DUP
	PUSHB[000]
	1
	SWAP
	PUSHB[000]
	10
	DIV
	FLOOR
	MUL

# Calculate right digit
# (VALUE%10 if VALUE<=99 else 1)

	DUP
	ROLL
	PUSHB[000]
	99
	MIN
	SWAP
	PUSHW[000]
	640
	MUL
	SUB
	PUSHW[000]
	-1
	MAX
	ABS

# Instruct x direction to the "88" configuration

	NPUSHB
	14
	25
	14
	22
	18
	20
	16
	12
	2
	10
	6
	8
	4
	0
	24

	SRP0
	MDRP[11110]
	MDRP[11101]
	ALIGNRP
	MDRP[11100]
	ALIGNRP
	MDRP[11101]
	MDRP[11110]
	MDRP[11101]
	ALIGNRP
	MDRP[11100]
	ALIGNRP
	MDRP[11101]
	MDRP[11110]

# Instruct y direction to the "88" configuration

	NPUSHB
	12
	2
	14
	10
	22
	8
	20
	18
	6
	16
	4
	12
	0

	SVTCA[0]
	MDAP[1]
	ALIGNRP
	MDRP[11101]
	ALIGNRP
	MDRP[11100]
	ALIGNRP
	MDRP[11101]
	ALIGNRP
	MDRP[11100]
	ALIGNRP
	MDRP[11101]
	ALIGNRP

# Depending on the digit, increase the size
# of the "holes" to change the visible shape.
# This is done by iterating through a loop twice,
# first for the left and then for the right digit

	NPUSHB
	29

# Values for the second loop iteration

	# Stop loop
	0

	# Points to align in y direction
	20
	18
	22
	14
	16
	12

	# Points to align in x direction
	22
	14
	18
	14
	20
	16
	12

# Values for the first loop iteration

	# Jump back to beginning of loop
	134
	1

	# Points to align in y direction
	8
	6
	10
	2
	4
	0

	# Points to align in x direction
	10
	2
	6
	2
	8
	4
	0

# Loop start
# (SB = Stack Bottom)

	# Align in x direction

		SVTCA[1]
		SRP0

	# if SB == 4 or SB == 5 or SB == 9

		DEPTH
		CINDEX
		DUP
		DUP
		PUSHB[000]
		4
		EQ
		SWAP
		PUSHB[000]
		5
		EQ
		OR
		SWAP
		PUSHB[000]
		9
		EQ
		OR
		IF
			DUP
			ALIGNRP
		EIF

	# if SB == 1 or SB == 3 or SB == 7

		DEPTH
		CINDEX
		DUP
		DUP
		PUSHB[000]
		1
		EQ
		SWAP
		PUSHB[000]
		3
		EQ
		OR
		SWAP
		PUSHB[000]
		7
		EQ
		OR
		IF
			ALIGNRP
			DUP
			ALIGNRP
		ELSE
			POP
		EIF

	# if SB == 2

		DEPTH
		CINDEX
		PUSHB[000]
		2
		EQ
		IF
			ALIGNRP
			SRP0
			ALIGNRP
		ELSE
			POP
			POP
			POP
		EIF

	# if SB == 5 or SB == 6

		SRP0

		DEPTH
		CINDEX
		DUP
		PUSHB[000]
		5
		EQ
		SWAP
		PUSHB[000]
		6
		EQ
		OR
		IF
			ALIGNRP
		ELSE
			POP
		EIF

	# Align in y direction

		SVTCA[0]
		SRP0

	# if SB == 7

		DEPTH
		CINDEX
		PUSHB[000]
		7
		EQ
		IF
			DUP
			ALIGNRP
		EIF

	# if SB == 1 or SB == 4 or SB > 9

		DEPTH
		CINDEX
		DUP
		DUP
		PUSHB[000]
		1
		EQ
		SWAP
		PUSHB[000]
		4
		EQ
		OR
		SWAP
		PUSHB[000]
		9
		GT
		OR
		IF
			ALIGNRP
			SRP0
			ALIGNRP
		ELSE
			POP
			POP
			POP
		EIF

	# if SB <= 1 or SB == 7
	# Note the use of MINDEX, which removes the value
	# from the bottom, and causes the second lowest
	# stack value to be used in the next loop iteration

		SRP0

		DEPTH
		MINDEX
		DUP
		PUSHB[000]
		1
		LTEQ
		SWAP
		PUSHB[000]
		7
		EQ
		OR
		IF
			ALIGNRP
		ELSE
			POP
		EIF

	# Check if the loop should be repeated

		IF
			NEG
			JMPR
		EIF

# Loop end

# Interpolate untouched points

	IUP[1]
	IUP[0]
